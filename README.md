# TrafficDrain Plug

This is the codification of the example `Plug` implementation as documented in [Botsquad's
GracefulStop Elixir Package](https://github.com/botsquad/graceful_stop)

The `Plug` provides a special endpoint named `__traffic` that will respond with an HTTP 200 OK
statuscode whilst the Phoenix Application is ready to handle requests. Whenever the beam process
has received a `SIGTERM` signal by k8s in order to go down, this `__traffic` endpoint will start
to respond with an HTTP 503 Service Unavailable statuscode.

By combining this behaviour with a kubernetes [readinessProbe](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-readiness-probes)
you will have a setup that is able to provide seamless upgrades of Phoenix Web Apps on Kubernetes.

## Credits

The original idea for capturing the `SIGTERM` signal and act accordingly is documented in [this blog
post](https://medium.com/@ellispritchard/graceful-shutdown-on-kubernetes-with-signals-erlang-otp-20-a22325e8ae98).
It has an accompanying [repo on github](https://github.com/Financial-Times/k8s_traffic_plug).

[Botsquad](https://www.botsquad.com) has implemented its [own version of this idea](https://github.com/botsquad/graceful_stop)
which is more flexible than its predecessor, but does not contain a `Plug`. However in the
[README](https://github.com/botsquad/graceful_stop/blob/master/README.md) there is an example,
written by [Vladimir Gorej](https://github.com/char0n), for a `Plug` which is the code we
have used to create this `TrafficDrain Plug`.

## Installation

Add this repository to your dependencies. As we've merely codified an example we think that we are
not the right people to publish this `Plug`, so if you want to use this `Plug` you'll have to use
a [git reference](https://hexdocs.pm/mix/Mix.Tasks.Deps.html#module-git-options-git).

```elixir
def deps do
  [
    ...,
    {:traffic_drain_plug, git: "https://gitlab.com/pandosearch/traffic_drain_plug", tag: "0.1"}
  ]
end
```

## Usage

Add this `Plug` in your [Phoenix Endpoint](https://hexdocs.pm/phoenix/Phoenix.Endpoint.html#content)
before your Router Plug. You don't need any of the router functionality, so it should run
**before** the Phoenix Router.

```elixir
defmodule YourApp.Endpoint do
  use Phoenix.Endpoint, otp_app: :your_app

  # plug ...
  # plug ...

  plug TrafficDrain.Plug
  plug YourApp.Router
end
```
