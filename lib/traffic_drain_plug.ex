defmodule TrafficDrain.Plug do
  @moduledoc """
  A `Plug` specific for usage as a kubernetes readinessProbe.


  This `Plug` only responds to HTTP requests for `/__traffic` and will usually respond with an
  HTTP 200 - OK statuscode. Once the BEAM process this Phoenix app is running in receives a
  `SIGTERM` signal, requests for `/__traffic` will respond with an HTTP 503 - Service Unavailable
  statuscode.

  ## Options

    * `:ready?` - If present, this function should return true when the BEAM process is ready to
      serve traffic. A typical use case is to check the database connection.
    * `:wait` - The time in milliseconds to wait before shutting down 503, defaults to 20_000ms.

  ## Example

      # In lib/your_app_web/endpoint.ex, befor any other plugs

      # Make the application wait for 30s before shutting down and
      # call `MyApp.ready?/0` before serving a 200 statuscode.
      plug TrafficDrain.Plug, wait: 30_000, ready?: &MyApp.ready?/0

      plug Plug.Static,
      ...

  ## Shutdown hooks

  This plug relies on Botsquad's [GracefulStop library](https://github.com/botsquad/graceful_stop)
  which you can configure to run additional hooks before shutdown.

  Each hook is identified by a 3-element `List` of which each element serves as a positioned
  parameter to `Task.async/3` `[module, function, arguments]` where `module` is the modulename in
  which to call `function` with `args`. Function is expected to be an `atom/0` and `args` is
  expected to be a `list()`.

  Hooks can be added in the configuration for the `graceful_stop` application, using the `:hooks`
  key. The value should be a `list/0` of hooks.

      # On shutdown Hookmodule.hook("arg1", "arg2") should be called
      config :graceful_stop, :hooks, [[Hookmodule, :hook, ["arg1", "arg2"]]

  """

  import Plug.Conn

  @behaviour Plug

  @impl true
  def init(opts \\ []) do
    opts = Keyword.validate!(opts, wait: 20_000, ready?: &__MODULE__.default_ready?/0)
    ready_fn = Keyword.get(opts, :ready?)

    if not is_function(ready_fn, 0) do
      raise ArgumentError, ":ready? must be a function with arity 0"
    end

    opts
  end

  def default_ready?(), do: true

  @impl true
  def call(%Plug.Conn{path_info: ["__traffic"]} = conn, opts) do
    # Time to wait before obeying the SIGTERM
    wait_in_ms = opts[:wait]

    # GracefulStop has its own overall timeout. If that is shorter than our wait +2.5s we'll
    # adjust the timeout of GracefulStop.
    timeout_in_ms = Application.get_env(:graceful_stop, :hook_timeout, 0)

    if wait_in_ms + 2500 > timeout_in_ms do
      Application.put_env(:graceful_stop, :hook_timeout, wait_in_ms + 2500)
    end

    # Add our waiting hook
    hooks =
      [
        [__MODULE__, :waiting_hook, [wait_in_ms]]
        | Application.get_env(:graceful_stop, :hooks, [])
      ]
      |> Enum.uniq()

    Application.put_env(:graceful_stop, :hooks, hooks)

    case GracefulStop.get_status() do
      :stopping ->
        conn
        |> put_resp_content_type("text/plain")
        |> send_resp(:service_unavailable, "Draining")
        |> halt()

      :running ->
        conn
        |> put_resp_content_type("text/plain")
        |> send_correct_response(opts)
        |> halt()
    end
  end

  @impl true
  def call(conn, _opts) do
    conn
  end

  @doc false
  @spec waiting_hook(non_neg_integer()) :: :ok
  def waiting_hook(wait_in_ms) do
    Process.sleep(wait_in_ms)
  end

  defp send_correct_response(conn, opts) do
    if opts[:ready?].() do
      send_resp(conn, :ok, "Serving")
    else
      send_resp(conn, :service_unavailable, "Not ready")
    end
  end
end
