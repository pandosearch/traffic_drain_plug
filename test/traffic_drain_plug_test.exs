defmodule TrafficDrain.PlugTest do
  use ExUnit.Case, async: true
  use Plug.Test

  alias TrafficDrain.Plug, as: TrafficDrainPlug

  setup do
    # Reset a GracefulStop.Handler.system_stop()
    send(:erl_signal_server, :resume)

    # Initialize the TrafficDrain.Plug
    opts = TrafficDrainPlug.init()

    # Configure the Conn with an HTTP Get to __traffic
    [conn: conn(:get, "/__traffic"), opts: opts]
  end

  test "Plug returns 200 while no SIGTERM received", %{conn: conn, opts: opts} do
    conn = TrafficDrainPlug.call(conn, opts)
    assert 200 == conn.status
  end

  test "Plug returns 503 once SIGTERM is received", %{conn: conn, opts: opts} do
    GracefulStop.Handler.system_stop()
    # Sleep to give system_stop/0 some time to kick in (as also done in GracefulStop tests)
    Process.sleep(100)

    conn = TrafficDrainPlug.call(conn, opts)
    assert 503 == conn.status
  end

  test "Plug returns 503 when ready? function returns false", %{conn: conn, opts: opts} do
    opts = Keyword.merge(opts, ready?: fn -> false end)
    conn = TrafficDrainPlug.call(conn, opts)
    assert 503 == conn.status
  end

  test "Plug raises when called with invalid ready? function" do
    assert_raise ArgumentError, ":ready? must be a function with arity 0", fn ->
      TrafficDrainPlug.init(ready?: :not_a_function)
    end

    assert_raise ArgumentError, ":ready? must be a function with arity 0", fn ->
      TrafficDrainPlug.init(ready?: fn _ -> false end)
    end
  end
end
